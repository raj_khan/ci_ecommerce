<?php


class Users extends CI_Controller
{


    public function register()
    {
        $this->load->model('User_model');
        $data['title'] = 'Sign Up';

        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('username', 'Username', 'required|callback_check_username_exists');
        $this->form_validation->set_rules('email', 'Email', 'required|callback_check_email_exists');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('password2', 'Password', 'matches[password]');

        if ($this->form_validation->run() === FALSE) {
            $this->load->view('component/header');
            $this->load->view('users/register', $data);
            $this->load->view('component/footer');
        } else {
//            Encrypt Password
            $enc_password = md5($this->input->post('password'));
            $this->User_model->register($enc_password);

//            Set Message
            $this->session->set_flashdata('user_registered', 'You are now registered and can log in');

            redirect('home');
        }

    }

    // Check if User Name Exists
    public function check_username_exists($username){
        $this->form_validation->set_message('Check_username_exists', 'That username is taken. Please Choose a different one');
        if($this->User_model->check_username_exists($username)){
            return true;
        }else{
            return false;
        }
    }

    // Check if Email  Exists
    public function check_email_exists($email){
        $this->form_validation->set_message('Check_email_exists', 'That email is taken. Please Choose a different one');
        if($this->User_model->check_email_exists($email)){
            return true;
        }else{
            return false;
        }
    }


    public function login(){
        $data['title'] = 'Log In';

        $this->form_validation->set_rules('username', 'Username','required');
        $this->form_validation->set_rules('email', 'Email','required');

        if($this->form_validation->run() === FALSE){
            $this->load->view('component/header');
            $this->load->view('users/login', $data);
            $this->load->view('component/footer');
        }else{
            // Get Username
            $username = $this->input->post('username');

            // Get and Encrypt Password
            $password = $this->input->post('password');

            // Login User
            $user_id = $this->user_model->login($username, $password);

            if($user_id){
                // Create Session
                $user_data = array(
                    'user_id' => $user_id,
                    'username' => $username,
                    'logged_in' => true
                );

                $this->session->set_userdata($user_data);

                // Set Message
                $this->session->set_flashdata('user_logged_in', 'You are now Logged In');
                redirect('home');
            }else{
                // Set Message
                $this->session->set_flashdata('loggin_failed', 'Login is Invalid');

                redirect('users/login');
            }
        }
    }
}