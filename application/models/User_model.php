<?php


class User_model extends CI_Model
{
    public function register($enc_password){
        // User Data array
        $data = array(
            'name' => $this->input->post('name'),
            'email' => $this->input->post('email'),
            'user_name' => $this->input->post('username'),
            'zipcode' => $this->input->post('zipcode'),
            'password' => $enc_password
        );

        // Insert User
        return $this->db->insert('users', $data);
    }



    // User Login
    public function login($username, $password){
        //Validate
        $this->db->where('user_name', $username);
        $this->db->where('password', $password);

        $result = $this->db->get('users');

        if($result->num_rows() == 1){
            return $result->row(0)->id;
        }else{
            return false;
        }
    }


    // Check Username exists
    public function check_username_exists($username){
        $query = $this->db->get_where('users', array('user_name' => $username));

        if(empty($query->row_array())){
            return true;
        }else{
            return false;
        }
    }


    // Check Email exists
    public function check_email_exists($email){
        $query = $this->db->get_where('users', array('email' => $email));

        if(empty($query->row_array())){
            return true;
        }else{
            return false;
        }
    }
}