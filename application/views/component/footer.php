<div class="sub-form-row">
    <div class="container">
        <div class="col-xs-12 col-sm-8 col-sm-offset-2 no-padding">
            <form role="form">
                <input placeholder="Subscribe to our newsletter">
                <button class="le-button">Subscribe</button>
            </form>
        </div>
    </div><!-- /.container -->
</div><!-- /.sub-form-row -->

<div class="link-list-row">
    <div class="container no-padding">
        <div class="col-xs-12 col-md-4 ">
            <!-- ============================================================= CONTACT INFO ============================================================= -->
            <div class="contact-info">
                <div class="footer-logo">
                    <img src="<?php echo base_url(); ?>assets/images/logo.png" alt="">
                </div><!-- /.footer-logo -->

                <p class="regular-bold"> Feel free to contact us via phone,email or just send us mail.</p>

                <p>
                    17 Princess Road, London, Greater London NW1 8JR, UK
                    1-888-8MEDIA (1-888-892-9953)
                </p>

                <div class="social-icons">
                    <h3>Get in touch</h3>
                    <ul>
                        <li><a href="http://facebook.com/transvelo" class="fa fa-facebook"></a></li>
                        <li><a href="#" class="fa fa-twitter"></a></li>
                        <li><a href="#" class="fa fa-pinterest"></a></li>
                        <li><a href="#" class="fa fa-linkedin"></a></li>
                        <li><a href="#" class="fa fa-stumbleupon"></a></li>
                        <li><a href="#" class="fa fa-dribbble"></a></li>
                        <li><a href="#" class="fa fa-vk"></a></li>
                    </ul>
                </div><!-- /.social-icons -->

            </div>
            <!-- ============================================================= CONTACT INFO : END ============================================================= -->
        </div>

        <div class="col-xs-12 col-md-8 no-margin">
            <!-- ============================================================= LINKS FOOTER ============================================================= -->
            <div class="link-widget">
                <div class="widget">
                    <h3>Find it fast</h3>
                    <ul>
                        <li><a href="category-grid.php">laptops &amp; computers</a></li>
                       
                    </ul>
                </div><!-- /.widget -->
            </div><!-- /.link-widget -->

            <div class="link-widget">
                <div class="widget">
                    <h3>Information</h3>
                    <ul>
                        <li><a href="category-grid.php">Find a Store</a></li>
                       

                    </ul>
                </div><!-- /.widget -->
            </div><!-- /.link-widget -->

            <div class="link-widget">
                <div class="widget">
                    <h3>Information</h3>
                    <ul>
                        <li><a href="category-grid.php">My Account</a></li>
                
                    </ul>
                </div><!-- /.widget -->
            </div><!-- /.link-widget -->
            <!-- ============================================================= LINKS FOOTER : END ============================================================= -->
        </div>
    </div><!-- /.container -->
</div><!-- /.link-list-row -->

<div class="copyright-bar">
    <div class="container">
        <div class="col-xs-12 col-sm-6 no-margin">
            <div class="copyright">
                &copy; <a href="<?php echo base_url(); ?>">Media Center</a> - all rights reserved
            </div><!-- /.copyright -->
        </div>
        <div class="col-xs-12 col-sm-6 no-margin">
            <div class="payment-methods ">
                <ul>
                    <li><img alt="" src="<?php echo base_url(); ?>assets/images/payments/payment-visa.png"></li>
                    <li><img alt="" src="<?php echo base_url(); ?>assets/images/payments/payment-master.png"></li>
                    <li><img alt="" src="<?php echo base_url(); ?>assets/images/payments/payment-paypal.png"></li>
                    <li><img alt="" src="<?php echo base_url(); ?>assets/images/payments/payment-skrill.png"></li>
                </ul>
            </div><!-- /.payment-methods -->
        </div>
    </div><!-- /.container -->
</div><!-- /.copyright-bar -->
</footer><!-- /#footer -->
<!-- ============================================================= FOOTER : END ============================================================= -->
</div><!-- /.wrapper -->

<!-- JavaScripts placed at the end of the document so the pages load faster -->
<script src="<?php echo base_url(); ?>assets/js/jquery-1.10.2.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery-migrate-1.2.1.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="http://maps.google.com/maps/api/js?key=AIzaSyDDZJO4F0d17RnFoi1F2qtw4wn6Wcaqxao&amp;sensor=false&amp;language=en"></script>
<script src="<?php echo base_url(); ?>assets/js/gmap3.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-hover-dropdown.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/owl.carousel.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/css_browser_selector.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/echo.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.easing-1.3.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-slider.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.raty.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.prettyPhoto.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.customSelect.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/wow.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/buttons.js"></script>
<script src="<?php echo base_url(); ?>assets/js/scripts.js"></script>

<!-- For demo purposes – can be removed on production -->
<script src="switchstylesheet/switchstylesheet.js"></script>

<script>
    $(document).ready(function () {
        $(".changecolor").switchstylesheet({seperator: "color"});
        $('.show-theme-options').click(function () {
            $(this).parent().toggleClass('open');
            return false;
        });
    });

    $(window).bind("load", function () {
        $('.show-theme-options').delay(2000).trigger('mouseover');
    });
</script>
<!-- For demo purposes – can be removed on production : End -->
</body>


</html>
