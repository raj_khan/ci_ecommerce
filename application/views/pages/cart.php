
			<!-- ============================================================= HEADER : END ============================================================= -->

            <div class="animate-dropdown">
                <!-- ========================================= BREADCRUMB ========================================= -->
                <div id="top-mega-nav">
                    <div class="container">
                        <nav>
                            <ul class="inline">
                                <li class="dropdown le-dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-list"></i> shop by department
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Computer Cases & Accessories</a></li>
                                        <li><a href="#">CPUs, Processors</a></li>
                                        <li><a href="#">Fans, Heatsinks &amp; Cooling</a></li>
                                        <li><a href="#">Graphics, Video Cards</a></li>
                                        <li><a href="#">Interface, Add-On Cards</a></li>
                                        <li><a href="#">Laptop Replacement Parts</a></li>
                                        <li><a href="#">Memory (RAM)</a></li>
                                        <li><a href="#">Motherboards</a></li>
                                        <li><a href="#">Motherboard &amp; CPU Combos</a></li>
                                        <li><a href="#">Motherboard Components</a></li>
                                    </ul>
                                </li>

                                <li class="breadcrumb-nav-holder">
                                    <ul>
                                        <li class="breadcrumb-item current gray">
                                            <a href="#">shopping cart</a>
                                        </li>
                                    </ul>
                                </li><!-- /.breadcrumb-nav-holder -->
                            </ul>
                        </nav>
                    </div><!-- /.container -->
                </div><!-- /#top-mega-nav -->
                <!-- ========================================= BREADCRUMB : END ========================================= -->
            </div>

            <section id="cart-page">
                <div class="container">
                    <!-- ========================================= CONTENT ========================================= -->
                    <div class="col-xs-12 col-md-9 items-holder no-margin">

                        <div class="row no-margin cart-item">
                            <div class="col-xs-12 col-sm-2 no-margin">
                                <a href="#" class="thumb-holder">
                                    <img class="lazy" alt="" src="<?php echo base_url(); ?>assets/images/products/product-small-01.jpg" />
                                </a>
                            </div>

                            <div class="col-xs-12 col-sm-5 ">
                                <div class="title">
                                    <a href="#">white lumia 9001</a>
                                </div>
                                <div class="brand">sony</div>
                            </div>

                            <div class="col-xs-12 col-sm-3 no-margin">
                                <div class="quantity">
                                    <div class="le-quantity">
                                        <form>
                                            <a class="minus" href="#reduce"></a>
                                            <input name="quantity" readonly="readonly" type="text" value="1" />
                                            <a class="plus" href="#add"></a>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-2 no-margin">
                                <div class="price">
                                    $2000.00
                                </div>
                                <a class="close-btn" href="#"></a>
                            </div>
                        </div><!-- /.cart-item -->

                        <div class="row no-margin cart-item">
                            <div class="col-xs-12 col-sm-2 no-margin">
                                <a href="#" class="thumb-holder">
                                    <img class="lazy" alt="" src="<?php echo base_url(); ?>assets/images/products/product-small-01.jpg" />
                                </a>
                            </div>

                            <div class="col-xs-12 col-sm-5">
                                <div class="title">
                                    <a href="#">white lumia 9001 </a>
                                </div>
                                <div class="brand">sony</div>
                            </div>

                            <div class="col-xs-12 col-sm-3 no-margin">
                                <div class="quantity">
                                    <div class="le-quantity">
                                        <form>
                                            <a class="minus" href="#reduce"></a>
                                            <input name="quantity" readonly="readonly" type="text" value="1" />
                                            <a class="plus" href="#add"></a>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-2 no-margin">
                                <div class="price">
                                    $2000.00
                                </div>
                                <a class="close-btn" href="#"></a>
                            </div>
                        </div><!-- /.cart-item -->

                        <div class="row no-margin cart-item">
                            <div class="col-xs-12 col-sm-2 no-margin">
                                <a href="#" class="thumb-holder">
                                    <img class="lazy" alt="" src="<?php echo base_url(); ?>assets/images/products/product-small-01.jpg" />
                                </a>
                            </div>

                            <div class="col-xs-12 col-sm-5">
                                <div class="title">
                                    <a href="#">white lumia 9001 </a>
                                </div>
                                <div class="brand">sony</div>
                            </div>

                            <div class="col-xs-12 col-sm-3 no-margin">
                                <div class="quantity">
                                    <div class="le-quantity">
                                        <form>
                                            <a class="minus" href="#reduce"></a>
                                            <input name="quantity" readonly="readonly" type="text" value="1" />
                                            <a class="plus" href="#add"></a>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-2 no-margin">
                                <div class="price">
                                    $2000.00
                                </div>
                                <a class="close-btn" href="#"></a>
                            </div>
                        </div><!-- /.cart-item -->

                        <div class="row no-margin cart-item">
                            <div class="col-xs-12 col-sm-2 no-margin">
                                <a href="#" class="thumb-holder">
                                    <img class="lazy" alt="" src="<?php echo base_url(); ?>assets/images/products/product-small-01.jpg" />
                                </a>
                            </div>

                            <div class="col-xs-12 col-sm-5">
                                <div class="title">
                                    <a href="#">white lumia 9001 </a>
                                </div>
                                <div class="brand">sony</div>
                            </div>

                            <div class="col-xs-12 col-sm-3 no-margin">
                                <div class="quantity">
                                    <div class="le-quantity">
                                        <form>
                                            <a class="minus" href="#reduce"></a>
                                            <input name="quantity" readonly="readonly" type="text" value="1" />
                                            <a class="plus" href="#add"></a>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-2 no-margin">
                                <div class="price">
                                    $2000.00
                                </div>
                                <a class="close-btn" href="#"></a>
                            </div>
                        </div><!-- /.cart-item -->
                    </div>
                    <!-- ========================================= CONTENT : END ========================================= -->

                    <!-- ========================================= SIDEBAR ========================================= -->

                    <div class="col-xs-12 col-md-3 no-margin sidebar ">
                        <div class="widget cart-summary">
                            <h1 class="border">shopping cart</h1>
                            <div class="body">
                                <ul class="tabled-data no-border inverse-bold">
                                    <li>
                                        <label>cart subtotal</label>
                                        <div class="value pull-right">$8434.00</div>
                                    </li>
                                    <li>
                                        <label>shipping</label>
                                        <div class="value pull-right">free shipping</div>
                                    </li>
                                </ul>
                                <ul id="total-price" class="tabled-data inverse-bold no-border">
                                    <li>
                                        <label>order total</label>
                                        <div class="value pull-right">$8434.00</div>
                                    </li>
                                </ul>
                                <div class="buttons-holder">
                                    <a class="le-button big" href="checkout.html" >checkout</a>
                                    <a class="simple-link block" href="category-grid.html" >continue shopping</a>
                                </div>
                            </div>
                        </div><!-- /.widget -->

                        <div id="cupon-widget" class="widget">
                            <h1 class="border">use coupon</h1>
                            <div class="body">
                                <form>
                                    <div class="inline-input">
                                        <input data-placeholder="enter coupon code" type="text" />
                                        <button class="le-button" type="submit">Apply</button>
                                    </div>
                                </form>
                            </div>
                        </div><!-- /.widget -->
                    </div><!-- /.sidebar -->

                    <!-- ========================================= SIDEBAR : END ========================================= -->
                </div>
            </section>

            <!-- ============================================================= FOOTER ============================================================= -->
            <footer id="footer" class="color-bg">
                <div class="container">
                    <div class="row no-margin widgets-row">
                        <div class="col-xs-12  col-sm-4 no-margin-left">
                            <!-- ============================================================= FEATURED PRODUCTS ============================================================= -->
                            <div class="widget">
                                <h2>Featured products</h2>
                                <div class="body">
                                    <ul>
                                        <li>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-9 no-margin">
                                                    <a href="single-product.html">Netbook Acer Travel B113-E-10072</a>.
                                                    <div class="price">
                                                        <div class="price-prev">$2000</div>
                                                        <div class="price-current">$1873</div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-3 no-margin">
                                                    <a href="#" class="thumb-holder">
                                                        <img alt="" src="<?php echo base_url(); ?>assets/images/blank.gif" data-echo="<?php echo base_url(); ?>assets/images/products/product-small-01.jpg" />
                                                    </a>
                                                </div>
                                            </div>
                                        </li>

                                        <li>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-9 no-margin">
                                                    <a href="single-product.html">PowerShot Elph 115 16MP Digital Camera</a>
                                                    <div class="price">
                                                        <div class="price-prev">$2000</div>
                                                        <div class="price-current">$1873</div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-3 no-margin">
                                                    <a href="#" class="thumb-holder">
                                                        <img alt="" src="<?php echo base_url(); ?>assets/images/blank.gif" data-echo="<?php echo base_url(); ?>assets/images/products/product-small-02.jpg" />
                                                    </a>
                                                </div>
                                            </div>
                                        </li>

                                        <li>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-9 no-margin">
                                                    <a href="single-product.html">PowerShot Elph 115 16MP Digital Camera</a>
                                                    <div class="price">
                                                        <div class="price-prev">$2000</div>
                                                        <div class="price-current">$1873</div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-3 no-margin">
                                                    <a href="#" class="thumb-holder">
                                                        <img alt="" src="<?php echo base_url(); ?>assets/images/blank.gif" data-echo="<?php echo base_url(); ?>assets/images/products/product-small-03.jpg" />
                                                    </a>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div><!-- /.body -->
                            </div> <!-- /.widget -->
                            <!-- ============================================================= FEATURED PRODUCTS : END ============================================================= -->
                        </div><!-- /.col -->

                        <div class="col-xs-12 col-sm-4 ">
                            <!-- ============================================================= ON SALE PRODUCTS ============================================================= -->
                            <div class="widget">
                                <h2>On-Sale Products</h2>
                                <div class="body">
                                    <ul>
                                        <li>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-9 no-margin">
                                                    <a href="single-product.html">HP Scanner 2910P</a>
                                                    <div class="price">
                                                        <div class="price-prev">$2000</div>
                                                        <div class="price-current">$1873</div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-3 no-margin">
                                                    <a href="#" class="thumb-holder">
                                                        <img alt="" src="<?php echo base_url(); ?>assets/images/blank.gif" data-echo="<?php echo base_url(); ?>assets/images/products/product-small-04.jpg" />
                                                    </a>
                                                </div>
                                            </div>

                                        </li>
                                        <li>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-9 no-margin">
                                                    <a href="single-product.html">Galaxy Tab 3 GT-P5210 16GB, Wi-Fi, 10.1in - White</a>
                                                    <div class="price">
                                                        <div class="price-prev">$2000</div>
                                                        <div class="price-current">$1873</div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-3 no-margin">
                                                    <a href="#" class="thumb-holder">
                                                        <img alt="" src="<?php echo base_url(); ?>assets/images/blank.gif" data-echo="<?php echo base_url(); ?>assets/images/products/product-small-05.jpg" />
                                                    </a>
                                                </div>
                                            </div>
                                        </li>

                                        <li>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-9 no-margin">
                                                    <a href="single-product.html">PowerShot Elph 115 16MP Digital Camera</a>
                                                    <div class="price">
                                                        <div class="price-prev">$2000</div>
                                                        <div class="price-current">$1873</div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-3 no-margin">
                                                    <a href="#" class="thumb-holder">
                                                        <img alt="" src="<?php echo base_url(); ?>assets/images/blank.gif" data-echo="<?php echo base_url(); ?>assets/images/products/product-small-06.jpg" />
                                                    </a>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div><!-- /.body -->
                            </div> <!-- /.widget -->
                            <!-- ============================================================= ON SALE PRODUCTS : END ============================================================= -->
                        </div><!-- /.col -->

                        <div class="col-xs-12 col-sm-4 ">
                            <!-- ============================================================= TOP RATED PRODUCTS ============================================================= -->
                            <div class="widget">
                                <h2>Top Rated Products</h2>
                                <div class="body">
                                    <ul>
                                        <li>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-9 no-margin">
                                                    <a href="single-product.html">Galaxy Tab GT-P5210, 10" 16GB Wi-Fi</a>
                                                    <div class="price">
                                                        <div class="price-prev">$2000</div>
                                                        <div class="price-current">$1873</div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-3 no-margin">
                                                    <a href="#" class="thumb-holder">
                                                        <img alt="" src="<?php echo base_url(); ?>assets/images/blank.gif" data-echo="<?php echo base_url(); ?>assets/images/products/product-small-07.jpg" />
                                                    </a>
                                                </div>
                                            </div>
                                        </li>

                                        <li>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-9 no-margin">
                                                    <a href="single-product.html">PowerShot Elph 115 16MP Digital Camera</a>
                                                    <div class="price">
                                                        <div class="price-prev">$2000</div>
                                                         <div class="price-current">$1873</div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-3 no-margin">
                                                    <a href="#" class="thumb-holder">
                                                        <img alt="" src="<?php echo base_url(); ?>assets/images/blank.gif" data-echo="<?php echo base_url(); ?>assets/images/products/product-small-08.jpg" />
                                                    </a>
                                                </div>
                                            </div>
                                        </li>

                                        <li>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-9 no-margin">
                                                    <a href="single-product.html">Surface RT 64GB, Wi-Fi, 10.6in - Dark Titanium</a>
                                                    <div class="price">
                                                        <div class="price-prev">$2000</div>
                                                        <div class="price-current">$1873</div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-3 no-margin">
                                                    <a href="#" class="thumb-holder">
                                                        <img alt="" src="<?php echo base_url(); ?>assets/images/blank.gif" data-echo="<?php echo base_url(); ?>assets/images/products/product-small-09.jpg" />
                                                    </a>
                                                </div>

                                            </div>
                                        </li>
                                    </ul>
                                </div><!-- /.body -->
                            </div><!-- /.widget -->
                            <!-- ============================================================= TOP RATED PRODUCTS : END ============================================================= -->
                        </div><!-- /.col -->
                    </div><!-- /.widgets-row-->
                </div><!-- /.container -->

               