
            <main id="contact-us" class="inner-bottom-md">
                <section class="google-map map-holder">
                    <div id="map" class="map center"></div>
                    <form role="form" class="get-direction">
                        <div class="container">
                            <div class="row">
                                <div class="center-block col-lg-10">
                                    <div class="input-group">
                                        <input type="text" class="le-input input-lg form-control" placeholder="Enter Your Starting Point">
                                        <span class="input-group-btn">
                                            <button class="btn btn-lg le-button" type="button">Get Directions</button>
                                        </span>
                                    </div><!-- /input-group -->
                                </div><!-- /.col-lg-6 -->
                            </div><!-- /.row -->
                        </div>
                    </form>
                </section>

                <div class="container">
                    <div class="row">

                        <div class="col-md-8">
                            <section class="section leave-a-message">
                                <h2 class="bordered">Leave a Message</h2>
                                <p>Maecenas dolor elit, semper a sem sed, pulvinar molestie lacus. Aliquam dignissim, elit non mattis ultrices, neque odio ultricies tellus, eu porttitor nisl ipsum eu massa.</p>
                                <form id="contact-form" class="contact-form cf-style-1 inner-top-xs" method="post" >
                                    <div class="row field-row">
                                        <div class="col-xs-12 col-sm-6">
                                            <label>Your Name*</label>
                                            <input class="le-input" >
                                        </div>
                                        <div class="col-xs-12 col-sm-6">
                                            <label>Your Email*</label>
                                            <input class="le-input" >
                                        </div>
                                    </div><!-- /.field-row -->

                                    <div class="field-row">
                                        <label>Subject</label>
                                        <input type="text" class="le-input">
                                    </div><!-- /.field-row -->

                                    <div class="field-row">
                                        <label>Your Message</label>
                                        <textarea rows="8" class="le-input"></textarea>
                                    </div><!-- /.field-row -->

                                    <div class="buttons-holder">
                                        <button type="submit" class="le-button huge">Send Message</button>
                                    </div><!-- /.buttons-holder -->
                                </form><!-- /.contact-form -->
                            </section><!-- /.leave-a-message -->
                        </div><!-- /.col -->

                        <div class="col-md-4">
                            <section class="our-store section inner-left-xs">
                                <h2 class="bordered">Our Store</h2>
                                <address>
                                    17 Princess Road <br/>
                                    London, Greater London <br/>
                                    NW1 8JR, UK
                                </address>
                                <h3>Hours of Operation</h3>
                                <ul class="list-unstyled operation-hours">
                                    <li class="clearfix">
                                        <span class="day">Monday:</span>
                                        <span class="pull-right hours">12-6 PM</span>
                                    </li>
                                    <li class="clearfix">
                                        <span class="day">Tuesday:</span>
                                        <span class="pull-right hours">12-6 PM</span>
                                    </li>
                                    <li class="clearfix">
                                        <span class="day">Wednesday:</span>
                                        <span class="pull-right hours">12-6 PM</span>
                                    </li>
                                    <li class="clearfix">
                                        <span class="day">Thursday:</span>
                                        <span class="pull-right hours">12-6 PM</span>
                                    </li>
                                    <li class="clearfix">
                                        <span class="day">Friday:</span>
                                        <span class="pull-right hours">12-6 PM</span>
                                    </li>
                                    <li class="clearfix">
                                        <span class="day">Saturday:</span>
                                        <span class="pull-right hours">12-6 PM</span>
                                    </li>
                                    <li class="clearfix">
                                        <span class="day">Sunday</span>
                                        <span class="pull-right hours">Closed</span>
                                    </li>
                                </ul>
                                <h3>Career</h3>
                                <p>If you're interested in employment opportunities at MediaCenter, please email us: <a href="mailto:contact@yourstore.com">contact@yourstore.com</a></p>
                            </section><!-- /.our-store -->
                        </div><!-- /.col -->

                    </div><!-- /.row -->
                </div><!-- /.container -->
            </main>
            <!-- ========================================= MAIN : END ========================================= -->

            <!-- ============================================================= FOOTER ============================================================= -->
            <footer id="footer" class="color-bg">
                <div class="container">
                    <div class="row no-margin widgets-row">
                        <div class="col-xs-12  col-sm-4 no-margin-left">
                            <!-- ============================================================= FEATURED PRODUCTS ============================================================= -->
                            <div class="widget">
                                <h2>Featured products</h2>
                                <div class="body">
                                    <ul>
                                        <li>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-9 no-margin">
                                                    <a href="single-product.html">Netbook Acer Travel B113-E-10072</a>.
                                                    <div class="price">
                                                        <div class="price-prev">$2000</div>
                                                        <div class="price-current">$1873</div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-3 no-margin">
                                                    <a href="#" class="thumb-holder">
                                                        <img alt="" src="assets/images/blank.gif" data-echo="assets/images/products/product-small-01.jpg" />
                                                    </a>
                                                </div>
                                            </div>
                                        </li>

                                        <li>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-9 no-margin">
                                                    <a href="single-product.html">PowerShot Elph 115 16MP Digital Camera</a>
                                                    <div class="price">
                                                        <div class="price-prev">$2000</div>
                                                        <div class="price-current">$1873</div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-3 no-margin">
                                                    <a href="#" class="thumb-holder">
                                                        <img alt="" src="assets/images/blank.gif" data-echo="assets/images/products/product-small-02.jpg" />
                                                    </a>
                                                </div>
                                            </div>
                                        </li>

                                        <li>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-9 no-margin">
                                                    <a href="single-product.html">PowerShot Elph 115 16MP Digital Camera</a>
                                                    <div class="price">
                                                        <div class="price-prev">$2000</div>
                                                        <div class="price-current">$1873</div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-3 no-margin">
                                                    <a href="#" class="thumb-holder">
                                                        <img alt="" src="assets/images/blank.gif" data-echo="assets/images/products/product-small-03.jpg" />
                                                    </a>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div><!-- /.body -->
                            </div> <!-- /.widget -->
                            <!-- ============================================================= FEATURED PRODUCTS : END ============================================================= -->
                        </div><!-- /.col -->

                        <div class="col-xs-12 col-sm-4 ">
                            <!-- ============================================================= ON SALE PRODUCTS ============================================================= -->
                            <div class="widget">
                                <h2>On-Sale Products</h2>
                                <div class="body">
                                    <ul>
                                        <li>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-9 no-margin">
                                                    <a href="single-product.html">HP Scanner 2910P</a>
                                                    <div class="price">
                                                        <div class="price-prev">$2000</div>
                                                        <div class="price-current">$1873</div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-3 no-margin">
                                                    <a href="#" class="thumb-holder">
                                                        <img alt="" src="assets/images/blank.gif" data-echo="assets/images/products/product-small-04.jpg" />
                                                    </a>
                                                </div>
                                            </div>

                                        </li>
                                        <li>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-9 no-margin">
                                                    <a href="single-product.html">Galaxy Tab 3 GT-P5210 16GB, Wi-Fi, 10.1in - White</a>
                                                    <div class="price">
                                                        <div class="price-prev">$2000</div>
                                                        <div class="price-current">$1873</div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-3 no-margin">
                                                    <a href="#" class="thumb-holder">
                                                        <img alt="" src="assets/images/blank.gif" data-echo="assets/images/products/product-small-05.jpg" />
                                                    </a>
                                                </div>
                                            </div>
                                        </li>

                                        <li>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-9 no-margin">
                                                    <a href="single-product.html">PowerShot Elph 115 16MP Digital Camera</a>
                                                    <div class="price">
                                                        <div class="price-prev">$2000</div>
                                                        <div class="price-current">$1873</div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-3 no-margin">
                                                    <a href="#" class="thumb-holder">
                                                        <img alt="" src="assets/images/blank.gif" data-echo="assets/images/products/product-small-06.jpg" />
                                                    </a>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div><!-- /.body -->
                            </div> <!-- /.widget -->
                            <!-- ============================================================= ON SALE PRODUCTS : END ============================================================= -->
                        </div><!-- /.col -->

                        <div class="col-xs-12 col-sm-4 ">
                            <!-- ============================================================= TOP RATED PRODUCTS ============================================================= -->
                            <div class="widget">
                                <h2>Top Rated Products</h2>
                                <div class="body">
                                    <ul>
                                        <li>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-9 no-margin">
                                                    <a href="single-product.html">Galaxy Tab GT-P5210, 10" 16GB Wi-Fi</a>
                                                    <div class="price">
                                                        <div class="price-prev">$2000</div>
                                                        <div class="price-current">$1873</div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-3 no-margin">
                                                    <a href="#" class="thumb-holder">
                                                        <img alt="" src="assets/images/blank.gif" data-echo="assets/images/products/product-small-07.jpg" />
                                                    </a>
                                                </div>
                                            </div>
                                        </li>

                                        <li>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-9 no-margin">
                                                    <a href="single-product.html">PowerShot Elph 115 16MP Digital Camera</a>
                                                    <div class="price">
                                                        <div class="price-prev">$2000</div>
                                                        <div class="price-current">$1873</div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-3 no-margin">
                                                    <a href="#" class="thumb-holder">
                                                        <img alt="" src="assets/images/blank.gif" data-echo="assets/images/products/product-small-08.jpg" />
                                                    </a>
                                                </div>
                                            </div>
                                        </li>

                                        <li>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-9 no-margin">
                                                    <a href="single-product.html">Surface RT 64GB, Wi-Fi, 10.6in - Dark Titanium</a>
                                                    <div class="price">
                                                        <div class="price-prev">$2000</div>
                                                        <div class="price-current">$1873</div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-3 no-margin">
                                                    <a href="#" class="thumb-holder">
                                                        <img alt="" src="assets/images/blank.gif" data-echo="assets/images/products/product-small-09.jpg" />
                                                    </a>
                                                </div>

                                            </div>
                                        </li>
                                    </ul>
                                </div><!-- /.body -->
                            </div><!-- /.widget -->
                            <!-- ============================================================= TOP RATED PRODUCTS : END ============================================================= -->
                        </div><!-- /.col -->
                    </div><!-- /.widgets-row-->
                </div><!-- /.container -->

