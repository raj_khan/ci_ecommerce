<div class="animate-dropdown">
    <!-- ========================================= BREADCRUMB ========================================= -->
    <div id="top-mega-nav">
        <div class="container">
            <nav>
                <ul class="inline">
                    <li class="dropdown le-dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-list"></i> shop by department
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="#">Computer Cases & Accessories</a></li>
                            <li><a href="#">CPUs, Processors</a></li>
                            <li><a href="#">Fans, Heatsinks &amp; Cooling</a></li>
                            <li><a href="#">Graphics, Video Cards</a></li>
                            <li><a href="#">Interface, Add-On Cards</a></li>
                            <li><a href="#">Laptop Replacement Parts</a></li>
                            <li><a href="#">Memory (RAM)</a></li>
                            <li><a href="#">Motherboards</a></li>
                            <li><a href="#">Motherboard &amp; CPU Combos</a></li>
                            <li><a href="#">Motherboard Components</a></li>
                        </ul>
                    </li>

                    <li class="breadcrumb-nav-holder">
                        <ul>
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url(); ?>">Home</a>
                            </li>
                            <li class="breadcrumb-item current gray">
                                <a >Authentication</a>
                            </li>
                        </ul>
                    </li><!-- /.breadcrumb-nav-holder -->
                </ul>
            </nav>
        </div><!-- /.container -->
    </div><!-- /#top-mega-nav -->
    <!-- ========================================= BREADCRUMB : END ========================================= -->
</div>

<!-- ========================================= MAIN ========================================= -->
<main id="authentication" class="inner-bottom-md">
    <div class="container">
        <div class="row">

            <div class="col-md-6">
                <section class="section sign-in inner-right-xs">
                    <h2 class="bordered">Sign In</h2>
                    <p>Hello, Welcome to your account</p>

                    <div class="social-auth-buttons">
                        <div class="row">
                            <div class="col-md-6">
                                <button class="btn-block btn-lg btn btn-facebook"><i class="fa fa-facebook"></i> Sign In
                                    with Facebook
                                </button>
                            </div>
                            <div class="col-md-6">
                                <button class="btn-block btn-lg btn btn-twitter"><i class="fa fa-twitter"></i> Sign In
                                    with Twitter
                                </button>
                            </div>
                        </div>
                    </div>
                    <?php echo validation_errors(); ?>

                    <?php echo form_open('users/register', 'class="register-form cf-style-1"'); ?>

                    <form role="form" class="login-form cf-style-1">
                        <div class="field-row">
                            <label>User Name</label>
                            <input type="text" name="username" class="le-input">
                        </div><!-- /.field-row -->

                        <div class="field-row">
                            <label>Password</label>
                            <input type="text" name="password" class="le-input">
                        </div><!-- /.field-row -->

                        <div class="field-row clearfix">
                                        <span class="pull-left">
                                            <label class="content-color"><input type="checkbox"
                                                                                class="le-checbox auto-width inline"> <span
                                                        class="bold">Remember me</span></label>
                                        </span>
                            <span class="pull-right">
                                            <a href="#" class="content-color bold">Forgotten Password ?</a>
                                        </span>
                        </div>

                        <div class="buttons-holder">
                            <button type="submit" class="le-button huge"> Sign In</button>
                        </div><!-- /.buttons-holder -->
                    </form><!-- /.cf-style-1 -->

                </section><!-- /.sign-in -->
            </div><!-- /.col -->

            <div class="col-md-6">
                <section class="section register inner-left-xs">
                    <h2 class="bordered">Create New Account</h2>
                    <p>Create your own Media Center account</p>
                    <?php echo form_open('users/register'); ?>
<!--                    <form role="form" class="register-form cf-style-1">-->

                        <!--Name-->
                        <div class="field-row">
                            <label>Name</label>
                            <input type="text" name="name" class="le-input">
                        </div><!-- /.Name -->

                        <!--User Name-->
                        <div class="field-row">
                            <label>User Name</label>
                            <input type="text" name="userName" class="le-input">
                        </div><!-- /.Name -->


                        <!--Email-->
                        <div class="field-row">
                            <label>Email</label>
                            <input type="email" name="email" class="le-input">
                        </div><!-- /.Name -->


                        <!--Zip Code-->
                        <div class="field-row">
                            <label>Zip Code</label>
                            <input type="number" name="zipCode" class="le-input">
                        </div><!-- /.Name -->


                        <!--Password-->
                        <div class="field-row">
                            <label>Password</label>
                            <input type="password" name="password" class="le-input">
                        </div><!-- /.Name -->



                        <div class="buttons-holder">
                            <button type="submit" class="le-button huge">Registration</button>
                        </div><!-- /.buttons-holder -->
                    </form>

                    <h2 class="semi-bold">Sign up today and you'll be able to :</h2>

                    <ul class="list-unstyled list-benefits">
                        <li><i class="fa fa-check primary-color"></i> Speed your way through the checkout</li>
                        <li><i class="fa fa-check primary-color"></i> Track your orders easily</li>
                        <li><i class="fa fa-check primary-color"></i> Keep a record of all your purchases</li>
                    </ul>

                </section><!-- /.register -->

            </div><!-- /.col -->

        </div><!-- /.row -->
    </div><!-- /.container -->
</main><!-- /.authentication -->
<!-- ========================================= MAIN : END ========================================= -->

<!-- ============================================================= FOOTER ============================================================= -->
<footer id="footer" class="color-bg">
    <div class="container">
        <div class="row no-margin widgets-row">
            <div class="col-xs-12  col-sm-4 no-margin-left">
                <!-- ============================================================= FEATURED PRODUCTS ============================================================= -->
                <div class="widget">
                    <h2>Featured products</h2>
                    <div class="body">
                        <ul>
                            <li>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-9 no-margin">
                                        <a href="single-product.html">Netbook Acer Travel B113-E-10072</a>.
                                        <div class="price">
                                            <div class="price-prev">$2000</div>
                                            <div class="price-current">$1873</div>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-3 no-margin">
                                        <a href="#" class="thumb-holder">
                                            <img alt="" src="assets/images/blank.gif"
                                                 data-echo="assets/images/products/product-small-01.jpg"/>
                                        </a>
                                    </div>
                                </div>
                            </li>

                            <li>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-9 no-margin">
                                        <a href="single-product.html">PowerShot Elph 115 16MP Digital Camera</a>
                                        <div class="price">
                                            <div class="price-prev">$2000</div>
                                            <div class="price-current">$1873</div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-3 no-margin">
                                        <a href="#" class="thumb-holder">
                                            <img alt="" src="assets/images/blank.gif"
                                                 data-echo="assets/images/products/product-small-02.jpg"/>
                                        </a>
                                    </div>
                                </div>
                            </li>

                            <li>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-9 no-margin">
                                        <a href="single-product.html">PowerShot Elph 115 16MP Digital Camera</a>
                                        <div class="price">
                                            <div class="price-prev">$2000</div>
                                            <div class="price-current">$1873</div>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-3 no-margin">
                                        <a href="#" class="thumb-holder">
                                            <img alt="" src="assets/images/blank.gif"
                                                 data-echo="assets/images/products/product-small-03.jpg"/>
                                        </a>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div><!-- /.body -->
                </div> <!-- /.widget -->
                <!-- ============================================================= FEATURED PRODUCTS : END ============================================================= -->
            </div><!-- /.col -->

            <div class="col-xs-12 col-sm-4 ">
                <!-- ============================================================= ON SALE PRODUCTS ============================================================= -->
                <div class="widget">
                    <h2>On-Sale Products</h2>
                    <div class="body">
                        <ul>
                            <li>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-9 no-margin">
                                        <a href="single-product.html">HP Scanner 2910P</a>
                                        <div class="price">
                                            <div class="price-prev">$2000</div>
                                            <div class="price-current">$1873</div>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-3 no-margin">
                                        <a href="#" class="thumb-holder">
                                            <img alt="" src="assets/images/blank.gif"
                                                 data-echo="assets/images/products/product-small-04.jpg"/>
                                        </a>
                                    </div>
                                </div>

                            </li>
                            <li>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-9 no-margin">
                                        <a href="single-product.html">Galaxy Tab 3 GT-P5210 16GB, Wi-Fi, 10.1in -
                                            White</a>
                                        <div class="price">
                                            <div class="price-prev">$2000</div>
                                            <div class="price-current">$1873</div>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-3 no-margin">
                                        <a href="#" class="thumb-holder">
                                            <img alt="" src="assets/images/blank.gif"
                                                 data-echo="assets/images/products/product-small-05.jpg"/>
                                        </a>
                                    </div>
                                </div>
                            </li>

                            <li>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-9 no-margin">
                                        <a href="single-product.html">PowerShot Elph 115 16MP Digital Camera</a>
                                        <div class="price">
                                            <div class="price-prev">$2000</div>
                                            <div class="price-current">$1873</div>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-3 no-margin">
                                        <a href="#" class="thumb-holder">
                                            <img alt="" src="assets/images/blank.gif"
                                                 data-echo="assets/images/products/product-small-06.jpg"/>
                                        </a>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div><!-- /.body -->
                </div> <!-- /.widget -->
                <!-- ============================================================= ON SALE PRODUCTS : END ============================================================= -->
            </div><!-- /.col -->

            <div class="col-xs-12 col-sm-4 ">
                <!-- ============================================================= TOP RATED PRODUCTS ============================================================= -->
                <div class="widget">
                    <h2>Top Rated Products</h2>
                    <div class="body">
                        <ul>
                            <li>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-9 no-margin">
                                        <a href="single-product.html">Galaxy Tab GT-P5210, 10" 16GB Wi-Fi</a>
                                        <div class="price">
                                            <div class="price-prev">$2000</div>
                                            <div class="price-current">$1873</div>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-3 no-margin">
                                        <a href="#" class="thumb-holder">
                                            <img alt="" src="assets/images/blank.gif"
                                                 data-echo="assets/images/products/product-small-07.jpg"/>
                                        </a>
                                    </div>
                                </div>
                            </li>

                            <li>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-9 no-margin">
                                        <a href="single-product.html">PowerShot Elph 115 16MP Digital Camera</a>
                                        <div class="price">
                                            <div class="price-prev">$2000</div>
                                            <div class="price-current">$1873</div>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-3 no-margin">
                                        <a href="#" class="thumb-holder">
                                            <img alt="" src="assets/images/blank.gif"
                                                 data-echo="assets/images/products/product-small-08.jpg"/>
                                        </a>
                                    </div>
                                </div>
                            </li>

                            <li>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-9 no-margin">
                                        <a href="single-product.html">Surface RT 64GB, Wi-Fi, 10.6in - Dark Titanium</a>
                                        <div class="price">
                                            <div class="price-prev">$2000</div>
                                            <div class="price-current">$1873</div>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-3 no-margin">
                                        <a href="#" class="thumb-holder">
                                            <img alt="" src="assets/images/blank.gif"
                                                 data-echo="assets/images/products/product-small-09.jpg"/>
                                        </a>
                                    </div>

                                </div>
                            </li>
                        </ul>
                    </div><!-- /.body -->
                </div><!-- /.widget -->
                <!-- ============================================================= TOP RATED PRODUCTS : END ============================================================= -->
            </div><!-- /.col -->
        </div><!-- /.widgets-row-->
    </div><!-- /.container -->

